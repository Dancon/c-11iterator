/****************************************************************************
**
** Copyright (C) 2017 Dancon <alexzavistovi4@yandex.by>.
** Contact: https://www.qt.io/licensing/
**
** This file is part of MyIterator
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#include <iostream>
#include <QDebug>

using namespace std;

#define START_POINT_OUTPUT true
#define END_POINT_OUTPUT true


template<typename T>
class _Array
{
    T* arr;
    int N;
public:
    class Iterator;

    _Array(int _N = 1) : N(_N)
    {
        arr = new T[N];
    }
    T& operator[](const int& n){
        if(n > 0 && n < N)
            return arr[n];
        if(n < 0 || n > N)
        qWarning() << "Warning!\nOut of range after using std::operator[" << n << "]\n";

        return arr[0];
    }

    Iterator begin(){
        return Iterator(this->arr);
    }
    Iterator end(){
        return Iterator(this->arr + N);
    }

    Iterator begin(bool flag){
        return Iterator(this->arr);
    }
    Iterator end(bool flag){
        return Iterator(this->arr + N - 1);
    }


    friend ostream& operator <<(ostream& s,const _Array<T>& _N);

    class Iterator
    {
        T* curr;
    public:
        Iterator(T* first) : curr(first){}

        T& operator+ (int n){ return *(curr + n); }
        T& operator- (int n){ return *(curr - n); }

        T& operator++ (int){ return *curr++; }
        T& operator-- (int){ return *curr--; }
        T& operator++ (){ return *++curr; }
        T& operator-- (){ return *--curr; }

        bool operator!= (const Iterator& it){ return curr != it.curr; }
        bool operator== (const Iterator& it){ return curr == it.curr; }
        T& operator* (){ return *curr; }
    };

};
template<typename T>
ostream operator <<(ostream &s, const _Array<T> &_N){
    s << _Array<T>::arr[_N];
    return s;
}


int main()
{
    _Array<int> arr(3);

    arr[0] = 1;
    arr[1] = 2;
    arr[2] = 4;

    /*  ERROR!!!OUT OF RANGE
#error    cout << *arr.begin() << endl;
#error    cout << *arr.end() << endl;
    */

    //GOOD
    cout << *arr.begin(START_POINT_OUTPUT) << endl;
    cout << *arr.end(END_POINT_OUTPUT) << endl;

    auto it = _Array<int>::Iterator(arr.begin());

    while(it != arr.end()){
        cout << *it << endl;
        it++;
    }

    return 0;
}

